package omellado.plugdelau;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import omellado.plugdelau.commands.CommandPrefix;
import omellado.plugdelau.commands.Home;
import omellado.plugdelau.commands.LauGamerCock;
import omellado.plugdelau.commands.Lobby;
import omellado.plugdelau.commands.SetHome;

public class PlugDeLau extends JavaPlugin {
	
	public String pathConfig;
	
	PluginDescriptionFile pdfFile = getDescription();
	
	public String version = pdfFile.getVersion();
	public String name = "[" + pdfFile.getName()+ "]";
	public String prefix = "plau";
	
	@Override
	public void onEnable() {
		Bukkit.getConsoleSender().sendMessage("-------------------------------------------------------");
		Bukkit.getConsoleSender().sendMessage("||\\      /\\      /\\      /||");
		Bukkit.getConsoleSender().sendMessage("||\\\\    //\\\\    //\\\\    //||");
		Bukkit.getConsoleSender().sendMessage("|| \\\\  //  \\\\  //  \\\\  // ||");
		Bukkit.getConsoleSender().sendMessage("||  \\\\//    \\\\//    \\\\//  ||");
		Bukkit.getConsoleSender().sendMessage("||   \\/      \\/      \\/   ||");
		Bukkit.getConsoleSender().sendMessage("||                        ||");
		Bukkit.getConsoleSender().sendMessage("||                        ||");
		Bukkit.getConsoleSender().sendMessage("||________________________||");
		Bukkit.getConsoleSender().sendMessage("____________________________");
		Bukkit.getConsoleSender().sendMessage("----------------------------");
		Bukkit.getConsoleSender().sendMessage("El plugin " + name + " a sido activado (version " + version + ")");
		Bukkit.getConsoleSender().sendMessage("-------------------------------------------------------");
		registCommands();
		registerConfig();
	}
	
	@Override
	public void onDisable() {
		Bukkit.getConsoleSender().sendMessage("-------------------------------------------------------");
		Bukkit.getConsoleSender().sendMessage("El plugin " + name + " a sido desactivado (version " + version + ")");
		Bukkit.getConsoleSender().sendMessage("-------------------------------------------------------");
	}
	
	public void registCommands() {
		this.getCommand("laugamer").setExecutor(new LauGamerCock(this));
		this.getCommand("sethome").setExecutor(new SetHome(this));
		this.getCommand("lobby").setExecutor(new Lobby(this));
		this.getCommand("home").setExecutor(new Home(this));
		this.getCommand(prefix).setExecutor(new CommandPrefix(this));
	}
	
	public void registerConfig() {
		File config = new File(this.getDataFolder(), "config.yml");
		pathConfig = config.getPath();
		
		if (!config.exists()) {
			this.getConfig().options().copyDefaults(true);
			saveConfig();
		}
	}
}

















