package omellado.plugdelau.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import omellado.plugdelau.PlugDeLau;

public class Lobby implements CommandExecutor {
	
	private PlugDeLau plugin;
	
	public Lobby(PlugDeLau plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Bukkit.getConsoleSender().sendMessage(plugin.name + " Como te voy a tepear al lobby si ni estai jugando xd");

			return false;
		} else {
			Player p = (Player) sender;
			
			FileConfiguration config = plugin.getConfig();
			World world = plugin.getServer().getWorld(config.getString("Config.Homes." + p + ".World"));
			
			if (p.getWorld().equals(world)) {
				
				p.sendMessage("Tepeando te pal lobby papito uwu");
				Location l = new Location(p.getWorld(), -177.5, 74.872, -235.5, 90, 0);
				p.teleport(l);
				
				return true;
			} else {
				p.sendMessage("No puedes tepearte entre dimensiones owo");
				
				return true;
			}
		}
	}
}
