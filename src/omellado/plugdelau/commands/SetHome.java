package omellado.plugdelau.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import omellado.plugdelau.PlugDeLau;

public class SetHome implements CommandExecutor {
	
	private PlugDeLau plugin;
	
	public SetHome(PlugDeLau plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Bukkit.getConsoleSender().sendMessage(plugin.name + " Vo ni teni casa aweonao jdskdjsk");
			return false;
		} else {
			Player p = (Player) sender;

			Location l = p.getLocation();
			String world = p.getWorld().getName();
			
			FileConfiguration config = plugin.getConfig();
			config.set("Config.Homes." + p + ".X", l.getX());
			config.set("Config.Homes." + p + ".Y", l.getY());
			config.set("Config.Homes." + p + ".Z", l.getZ());
			config.set("Config.Homes." + p + ".Yaw", l.getYaw());
			config.set("Config.Homes." + p + ".Pitch", l.getPitch());
			config.set("Config.Homes." + p + ".World", world);
			plugin.saveConfig();
			p.sendMessage("Tu casita se a guardado correctamente uwu");
			
			return true;
		}
	}
}
