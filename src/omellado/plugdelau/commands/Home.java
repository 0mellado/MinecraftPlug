package omellado.plugdelau.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import omellado.plugdelau.PlugDeLau;

public class Home implements CommandExecutor {
	
	private PlugDeLau plugin;
	
	public Home(PlugDeLau plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			Bukkit.getConsoleSender().sendMessage(plugin.name + " Ese comando solo lo puede usar un jugador, tonto jijiji");
			
			return false;
		} else {

			Player p = (Player) sender;
	
			FileConfiguration config = plugin.getConfig();
			double x = Double.valueOf(config.getString("Config.Homes." + p + ".X"));
			double y = Double.valueOf(config.getString("Config.Homes." + p + ".Y"));
			double z = Double.valueOf(config.getString("Config.Homes." + p + ".Z"));
			float yaw = Float.valueOf(config.getString("Config.Homes." + p + ".Yaw"));
			float pitch = Float.valueOf(config.getString("Config.Homes." + p + ".Pitch"));
			World world = plugin.getServer().getWorld(config.getString("Config.Homes." + p + ".World"));

			if (p.getWorld().equals(world)) {
				Location l = new Location(world, x, y, z, yaw, pitch);
				p.teleport(l);
				
				return true;
			} else {
				Bukkit.getConsoleSender().sendMessage("No puedes tepear entre dimensiones");
				
				return true;
			}
		}
	}
}
